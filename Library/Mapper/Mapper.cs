﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Library.Mapper
{
    public static class Mapper
    {
        // http://stackoverflow.com/a/16816463
        private static Dictionary<KeyValuePair<Type, Type>, object> _maps = new Dictionary<KeyValuePair<Type, Type>, object>();

        private static PropertyInfo[] TFromProperties;
        private static PropertyInfo[] TToProperties;
        private static FieldInfo[] TFromFields;
        private static FieldInfo[] TToFields;

        public static void AddMap<TFrom, TTo>(Func<TFrom, TTo> Map = null)
            where TFrom : class
            where TTo : class
        {
            _maps.Add(new KeyValuePair<Type, Type>(typeof(TFrom), typeof(TTo)), Map);
            if (Map == null)
                _maps.Add(new KeyValuePair<Type, Type>(typeof(TTo), typeof(TFrom)), Map);
        }

        public static ToType Map<FromType, ToType>(FromType From)
        {
            var key = new KeyValuePair<Type, Type>(typeof(FromType), typeof(ToType));
            var map = (Func<FromType, ToType>)_maps[key];

            var hasMapping = _maps.Any(x => x.Key.Equals(key));

            if (!hasMapping)
                throw new Exception(
                    string.Format("No map defined for {0} => {1}",
                        typeof(FromType).Name, typeof(ToType).Name));

            var tFrom = typeof(FromType);
            var tTo = typeof(ToType);

            TFromProperties = tFrom.GetProperties();
            TFromFields = tFrom.GetFields();
            TToProperties = tTo.GetProperties();
            TToFields = tTo.GetFields();

            var toObj = (ToType)Activator.CreateInstance(typeof(ToType));
            SyncProperties(From, toObj);
            SyncFields(From, toObj);

            // Sync and mapped data, override anything that auto synced with mapping action.
            if (map != null)
                return map(From);
            else
                return toObj;
        }

        private static void SyncProperties<FromType, ToType>(FromType objFrom, ToType objTo)
        {
            var fromProperties = TFromProperties;
            var toProperties = TToProperties;
            var toFields = TToFields;

            if (fromProperties != null && fromProperties.Count() > 0)
            {
                foreach (var fromProperty in fromProperties)
                {
                    if (toProperties.Any(x => x.Name == fromProperty.Name))
                    {
                        var destinationProperty = toProperties.Where(x => x.Name == fromProperty.Name).FirstOrDefault();

                        if (MatchingProps(fromProperty, destinationProperty))
                        {
                            var val = fromProperty.GetValue(objFrom, null);
                            destinationProperty.SetValue(objTo, Convert.ChangeType(val, fromProperty.PropertyType), null);
                        }
                    }

                    if (toFields.Any(x => x.Name == fromProperty.Name))
                    {
                        var destinationField = toFields.Where(x => x.Name == fromProperty.Name).FirstOrDefault();

                        if (MatchingPropertyToField(fromProperty, destinationField))
                        {
                            var val = fromProperty.GetValue(objFrom, null);
                            destinationField.SetValue(objTo, val);
                        }
                    }
                }
            }
        }

        private static void SyncFields<FromType, ToType>(FromType objFrom, ToType objTo)
        {
            var fromFields = TFromFields;
            var toFields = TToFields;
            var toProperties = TToProperties;

            if (fromFields != null && fromFields.Count() > 0)
            {
                foreach (var fromField in fromFields)
                {
                    if (toFields.Any(x => x.Name == fromField.Name))
                    {
                        var destinationField = toFields.Where(x => x.Name == fromField.Name).FirstOrDefault();

                        if (MatchingFields(fromField, destinationField))
                        {
                            var val = fromField.GetValue(objFrom);
                            destinationField.SetValue(objTo, val);
                        }
                    }

                    if (toProperties.Any(x => x.Name == fromField.Name))
                    {
                        var destinationProperty = toProperties.Where(x => x.Name == fromField.Name).FirstOrDefault();

                        if (MatchingFieldToProperty(fromField, destinationProperty))
                        {
                            var val = fromField.GetValue(objFrom);
                            destinationProperty.SetValue(objTo, val, null);
                        }
                    }
                }
            }
        }

        // Rules...
        static Func<PropertyInfo, PropertyInfo, bool> MatchingProps = (T1, T2) => T1.Name == T2.Name && T1.PropertyType.Name == T2.PropertyType.Name;
        static Func<FieldInfo, FieldInfo, bool> MatchingFields = (T1, T2) => T1.Name == T2.Name && T1.FieldType.Name == T2.FieldType.Name;
        static Func<PropertyInfo, FieldInfo, bool> MatchingPropertyToField = (T1, T2) => T1.Name == T2.Name && T1.PropertyType.Name == T2.FieldType.Name;
        static Func<FieldInfo, PropertyInfo, bool> MatchingFieldToProperty = (T1, T2) => T1.Name == T2.Name && T1.FieldType.Name == T2.PropertyType.Name;

    }
}