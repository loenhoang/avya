﻿using System.ComponentModel;

namespace Library.Enum
{
    public enum MoviePlatformCode
    {
        None = 1,
        [Description("Amazon Prime")]
        AmazonPrime,
        DVD,
        Hulu,
        Netflix,
        Other,
        VHS,
        Youtube
    }
}
