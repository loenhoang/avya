﻿using System;
using System.ComponentModel;

namespace Library.Enum
{
    [Flags]
    public enum MediaFormCode
    {
        [Description("N/A")]
        NotApplicable = 1,
        Digital,
        Physical
    }
}
