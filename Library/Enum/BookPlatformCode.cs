﻿using System.ComponentModel;

namespace Library.Enum
{
    public enum BookPlatformCode
    {
        None = 1,
        [Description("Google Books")]
        GoogleBooks,
        iBook,
        Kindle,
        Kobo,
        Physical,
        [Description("Sony Reader")]
        SonyReader
    }
}
