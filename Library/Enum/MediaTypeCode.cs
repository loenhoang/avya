﻿namespace Library.Enum
{
    public enum MediaTypeCode
    {
        Book = 1,
        Game,
        Movie
    }
}
