﻿namespace Library.Enum
{
    public enum SortCode
    {
        Fav,
        Items,
        Name,
        Platform,
        Rating
    }

    public enum OrderByCode
    {
        Asc,
        Desc
    }
}
