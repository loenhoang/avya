﻿using System.ComponentModel;

namespace Library.Enum
{
    public enum GamePlatformCode
    {
        None = 1,
        Gameboy,
        [Description("Gameboy Colour")]
        GameboyColour,
        [Description("Gameboy Advance")]
        GameboyAdvance,
        GameboyAdvanceSP,
        DS,
        [Description("3DS")]
        ThreeDS,
        [Description("3DS XL")]
        ThreeDSXL,
        SNES,
        N64,
        Gamecube,
        Wii,
        WiiU,
        [Description("Game Gear")]
        GameGear,
        [Description("Master System")]
        MasterSystem,
        Megadrive,
        [Description("Megadrive 2")]
        MegadriveTwo,
        Saturn,
        Dreamcast,
        [Description("PC-DVD")]
        PCDVD,
        PSN,
        PSP,
        [Description("PS Vita")]
        PSVita,
        PSX,
        PS2,
        PS3,
        PS4,
        Origin,
        uPlay,
        Steam,
        XBOX,
        [Description("XBOX 360")]
        XBOX360,
        [Description("XBOX One")]
        XBOX1
    }
}
