﻿using System;
using System.ComponentModel;

namespace Library.Enum
{
    [Flags]
    public enum GenreCode
    {
        None = 1,
        Action,
        Animation,
        Comedy,
        Adventure,
        Biographical,
        Horror,
        [Description("Sci-Fi")]
        Scifi,
        Thriller
    }
}
