﻿using System.Globalization;

namespace Library.Extension
{
    public static class ObjectExtensions
    {
        public static bool IsWithinRange(this int num, int start, int end)
        {
            return start <= num && num <= end;
        }
        public static string ToTitleCase(this string input)
        {
            TextInfo textInfo = new CultureInfo("en-GB", false).TextInfo;
            var result = textInfo.ToTitleCase(input);

            return result;
        }
    }
}
