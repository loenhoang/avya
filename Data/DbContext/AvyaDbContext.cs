﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Data.DTO;
using Data.Entity;
using Data.Extension;
using Library.Enum;
using Microsoft.AspNet.Identity.EntityFramework;

namespace Data.DbContext
{
    public class AvyaDbContext : ApplicationDbContext
    {
        public static new AvyaDbContext Create()
        {
            return new AvyaDbContext();
        }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.MapEntityProperties<Collection>();
            modelBuilder.MapEntityProperties<CollectionDetail>();
            modelBuilder.MapEntityProperties<Book>();
            modelBuilder.MapEntityProperties<Game>();
            modelBuilder.MapEntityProperties<Movie>();

            modelBuilder.Entity<Collection>().Ignore(x => x.ItemCount).HasMany(x => x.CollectionDetails).WithRequired(x => x.Collection).HasForeignKey(c => c.CollectionId).WillCascadeOnDelete(true);
            modelBuilder.Entity<BookPlatform>().HasMany(x => x.Books);
            modelBuilder.Entity<GamePlatform>().HasMany(x => x.Games);
            modelBuilder.Entity<MediaType>().HasMany(x => x.Collection);
            modelBuilder.Entity<MoviePlatform>().HasMany(x => x.Movies);

            modelBuilder.Entity<ApplicationUser>().ToTable("Users", "dbo");
            modelBuilder.Entity<IdentityRole>().ToTable("Roles", "dbo");
            modelBuilder.Entity<IdentityUserRole>().ToTable("UserRoles", "dbo");
            modelBuilder.Entity<IdentityUserClaim>().ToTable("UserClaims", "dbo");
            modelBuilder.Entity<IdentityUserLogin>().ToTable("UserLogins", "dbo");

            modelBuilder.Ignore<BaseEntity>();
            modelBuilder.Ignore<BaseItem>();
        }

        public DbSet<Book> Books { get; set; }
        public DbSet<Collection> Collections { get; set; }
        public DbSet<CollectionDetail> CollectionDetails { get; set; }
        public DbSet<Game> Games { get; set; }
        public DbSet<Genre> Genres { get; set; }
        public DbSet<MediaForm> MediaForms { get; set; }
        public DbSet<MediaType> MediaTypes { get; set; }
        public DbSet<Movie> Movies { get; set; }
        public DbSet<BookPlatform> BookPlatforms { get; set; }
        public DbSet<GamePlatform> GamePlatforms { get; set; }
        public DbSet<MoviePlatform> MoviePlatforms { get; set; }

        public IEnumerable<CollectionDTO> GetAllUserCollections(string userId)
        {
            var results = from c in Collections
                          join cd in CollectionDetails
                          on c.Id equals cd.CollectionId
                          into joined
                          where c.UserId == userId
                          select new CollectionDTO
                          {
                              Id = c.Id,
                              CollectionType = c.CollectionType,
                              Favourite = c.Favourite,
                              Name = c.Name,
                              UserId = c.UserId,
                              ItemCount = joined.Count(cd => cd.CollectionId == c.Id)
                          };

            return results;
        }

        public IEnumerable<CollectionDetail> GetAllCollectionDetails(int colId, MediaTypeCode type)
        {
            var results = CollectionDetails.Where(x => x.CollectionId == colId)
                .Join(this.CollectionByType(type), cd => cd.ItemId, b => b.Id,
                    (cd, b) => new
                    {
                        cd.Id,
                        cd.CollectionId,
                        cd.Rating,
                        cd.BookPlatform,
                        cd.GamePlatform,
                        cd.MoviePlatform,
                        b
                    })
                    .AsEnumerable()
                    .Select(a => new CollectionDetail
                    {
                        Id = a.Id,
                        CollectionId = a.CollectionId,
                        ItemId = a.b.Id,
                        ItemName = a.b.Name,
                        Rating = a.Rating,
                        BookPlatform = a.BookPlatform,
                        GamePlatform = a.GamePlatform,
                        MoviePlatform = a.MoviePlatform
                    });

            return results;
        }

    }

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }
    }
}