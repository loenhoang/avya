﻿using Library.Enum;

namespace Data.Entity
{
    public class Movie : BaseItem
    {
        public string Cast { get; set; }
        public string Director { get; set; }
        public MoviePlatformCode AvailablePlatforms { get; set; }
    }
}
