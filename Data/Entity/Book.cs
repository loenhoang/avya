﻿using Library.Enum;

namespace Data.Entity
{
    public class Book : BaseItem
    {
        public int ISBN { get; set; }
        public string Author { get; set; }
        public BookPlatformCode AvailablePlatforms { get; set; }
    }
}
