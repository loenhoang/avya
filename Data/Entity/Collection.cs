﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Library.Enum;

namespace Data.Entity
{
    public class Collection : BaseEntity
    {
        public bool Favourite { get; set; }

        public MediaTypeCode CollectionType { get; set; }
        public ICollection<CollectionDetail> CollectionDetails { get; set; }

        public string UserId { get; set; }
        public int ItemCount { get; set; }

        [ForeignKey("UserId")]
        public virtual ApplicationUser User { get; set; }
    }
}
