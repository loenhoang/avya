﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Library.Enum;

namespace Data.Entity
{
    public class MoviePlatform
    {
        [Key]
        public MoviePlatformCode Code { get; set; }
        public string Description { get; set; }
        public virtual ICollection<Movie> Movies { get; set; }
    }
}
