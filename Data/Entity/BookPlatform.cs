﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Library.Enum;

namespace Data.Entity
{
    public class BookPlatform
    {
        [Key]
        public BookPlatformCode Code { get; set; }
        public string Description { get; set; }
        public virtual ICollection<Book> Books { get; set; }
    }
}
