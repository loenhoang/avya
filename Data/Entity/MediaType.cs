﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Library.Enum;

namespace Data.Entity
{
    public class MediaType
    {
        [Key]
        public MediaTypeCode Code { get; set; }
        public string Description { get; set; }
        public virtual ICollection<Collection> Collection { get; set; }
    }
}
