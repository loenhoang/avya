﻿using System.ComponentModel.DataAnnotations;
using Library.Enum;

namespace Data.Entity
{
    public class Genre
    {
        [Key]
        public GenreCode Code { get; set; }
        public string Description { get; set; }
    }
}
