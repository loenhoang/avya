﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Library.Enum;

namespace Data.Entity
{
    public class GamePlatform
    {
        [Key]
        public GamePlatformCode Code { get; set; }
        public string Description { get; set; }
        public virtual ICollection<Game> Games { get; set; }
    }
}
