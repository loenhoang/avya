﻿using System.ComponentModel.DataAnnotations.Schema;
using Library.Enum;

namespace Data.Entity
{
    public class CollectionDetail : BaseEntity
    {
        public int Rating { get; set; }
        public BookPlatformCode BookPlatform { get; set; }
        public GamePlatformCode GamePlatform { get; set; }
        public MoviePlatformCode MoviePlatform { get; set; }
        public int ItemId { get; set; }
        public string ItemName { get; set; }

        public int CollectionId { get; set; }
        [ForeignKey("CollectionId")]
        public virtual Collection Collection { get; set; }
    }
}
