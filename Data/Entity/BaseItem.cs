﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Library.Enum;

namespace Data.Entity
{
    public class BaseItem : BaseEntity
    {
        public string Description { get; set; }
        public DateTime? ReleaseDate { get; set; }
        public GenreCode Genres { get; set; }
        public MediaFormCode MediaForm { get; set; }
    }
}
