﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Library.Enum;

namespace Data.Entity
{
    public class MediaForm
    {
        [Key]
        public MediaFormCode Code { get; set; }
        public string Description { get; set; }
        public ICollection<BaseItem> Items { get; set; }
    }
}
