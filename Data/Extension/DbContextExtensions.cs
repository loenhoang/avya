﻿using System.Collections.Generic;
using Data.DbContext;
using Data.Entity;
using Library.Enum;

namespace Data.Extension
{
    public static class DbContextExtensions
    {
        public static IEnumerable<BaseEntity> CollectionByType(this AvyaDbContext ctx, MediaTypeCode type)
        {
            if (type == MediaTypeCode.Book)
            {
                return ctx.Books;
            }
            else if (type == MediaTypeCode.Game)
            {
                return ctx.Games;
            }
            else { return ctx.Movies; }
        }
    }
}
