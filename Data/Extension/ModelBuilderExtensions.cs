﻿using System.Data.Entity;
using Data.Entity;

namespace Data.Extension
{
    public static class ModelBuilderExtensions
    {
        public static void MapEntityProperties<T>(this DbModelBuilder modelBuilder, string tableName = null) where T : BaseEntity
        {
            dynamic type = typeof(T);
            modelBuilder.Entity<T>().HasKey(x => x.Id);

            modelBuilder.Entity<T>().Map(x =>
            {
                x.MapInheritedProperties();
                x.ToTable(tableName ?? type.Name);
            });
        }
    }
}
