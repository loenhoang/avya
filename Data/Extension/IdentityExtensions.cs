﻿using System.Security.Claims;
using System.Security.Principal;

namespace Data.Extension
{
    public static class IdentityExtensions
    {
        public static string GetProfileImage(this IIdentity identity)
        {
            var claim = ((ClaimsIdentity)identity).FindFirst("ProfileImage");

            // Test for null to avoid issues during local testing
            return (claim != null) ? claim.Value : string.Empty;
        }
    }
}
