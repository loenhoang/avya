namespace Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class favcollection : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Collection", "Favourite", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Collection", "Favourite");
        }
    }
}
