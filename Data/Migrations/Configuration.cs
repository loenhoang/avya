namespace Data.Migrations
{
    using System.Data.Entity.Migrations;
    using DbContext;
    internal sealed class Configuration : DbMigrationsConfiguration<AvyaDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(AvyaDbContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //context.Collections.AddOrUpdate(
            //  c => c.Id,
            //  new Collection
            //  {
            //      Id = 1,
            //      Name = "Test Collection",
            //      UserId = "cebc258a-b3ca-47be-9835-b8339abfca29",
            //      CollectionType = MediaTypeCode.Book
            //  }
            //);

            //context.CollectionDetails.AddOrUpdate(
            //    cd => cd.Id,
            //    new CollectionDetail
            //    {
            //        Id = 1,
            //        CollectionId = 1,
            //        ItemId = 1,
            //        ItemName = "Test Book",
            //        BookPlatform = BookPlatformCode.Kindle,
            //        Rating = 10,
            //        GamePlatform = GamePlatformCode.None,
            //        MoviePlatform = MoviePlatformCode.None
            //    }
            //);

            //context.Books.AddOrUpdate(
            //    b => b.Id,
            //    new Book
            //    {
            //        Id = 1,
            //        Author = "Test Author",
            //        Description = "Test Description",
            //        Genres = GenreCode.Comedy | GenreCode.Action,
            //        ISBN = 1,
            //        Name = "Test Book",
            //        ReleaseDate = DateTime.UtcNow,
            //        MediaForm = MediaFormCode.Digital,
            //        AvailablePlatforms = BookPlatformCode.Kindle | BookPlatformCode.GoogleBooks
            //    });

            //context.Genres.AddOrUpdate(
            //    g => g.Code,
            //    new Genre
            //    {
            //        Code = GenreCode.None,
            //        Description = "None"
            //    });
            //context.Genres.AddOrUpdate(
            //    g => g.Code,
            //    new Genre
            //    {
            //        Code = GenreCode.Action,
            //        Description = "Action"
            //    });
            //context.Genres.AddOrUpdate(
            //    g => g.Code,
            //    new Genre
            //    {
            //        Code = GenreCode.Comedy,
            //        Description = "Comedy"
            //    });

            //context.MediaForms.AddOrUpdate(
            //    mf => mf.Code,
            //    new MediaForm
            //    {
            //        Code = MediaFormCode.Digital,
            //        Description = "Digital"
            //    });

            //context.BookPlatforms.AddOrUpdate(
            //    bp => bp.Code,
            //    new BookPlatform
            //    {
            //        Code = BookPlatformCode.Kindle,
            //        Description = "Kindle"
            //    });
        }
    }
}
