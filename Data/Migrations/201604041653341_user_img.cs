namespace Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class user_img : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Users", "ProfileImage", c => c.Binary());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Users", "ProfileImage");
        }
    }
}
