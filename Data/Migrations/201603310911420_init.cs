namespace Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class init : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.BookPlatforms",
                c => new
                    {
                        Code = c.Int(nullable: false),
                        Description = c.String(),
                    })
                .PrimaryKey(t => t.Code);
            
            CreateTable(
                "dbo.Book",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ISBN = c.Int(nullable: false),
                        Author = c.String(),
                        AvailablePlatforms = c.Int(nullable: false),
                        Description = c.String(),
                        ReleaseDate = c.DateTime(),
                        Genres = c.Int(nullable: false),
                        MediaForm = c.Int(nullable: false),
                        Name = c.String(),
                        BookPlatform_Code = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.BookPlatforms", t => t.BookPlatform_Code)
                .Index(t => t.BookPlatform_Code);
            
            CreateTable(
                "dbo.CollectionDetail",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Rating = c.Int(nullable: false),
                        BookPlatform = c.Int(nullable: false),
                        GamePlatform = c.Int(nullable: false),
                        MoviePlatform = c.Int(nullable: false),
                        ItemId = c.Int(nullable: false),
                        ItemName = c.String(),
                        CollectionId = c.Int(nullable: false),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Collection", t => t.CollectionId, cascadeDelete: true)
                .Index(t => t.CollectionId);
            
            CreateTable(
                "dbo.Collection",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CollectionType = c.Int(nullable: false),
                        UserId = c.String(maxLength: 128),
                        Name = c.String(),
                        MediaType_Code = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.UserId)
                .ForeignKey("dbo.MediaTypes", t => t.MediaType_Code)
                .Index(t => t.UserId)
                .Index(t => t.MediaType_Code);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        FirstName = c.String(),
                        Surname = c.String(),
                        Email = c.String(maxLength: 256),
                        EmailConfirmed = c.Boolean(nullable: false),
                        PasswordHash = c.String(),
                        SecurityStamp = c.String(),
                        PhoneNumber = c.String(),
                        PhoneNumberConfirmed = c.Boolean(nullable: false),
                        TwoFactorEnabled = c.Boolean(nullable: false),
                        LockoutEndDateUtc = c.DateTime(),
                        LockoutEnabled = c.Boolean(nullable: false),
                        AccessFailedCount = c.Int(nullable: false),
                        UserName = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.UserName, unique: true, name: "UserNameIndex");
            
            CreateTable(
                "dbo.UserClaims",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.String(nullable: false, maxLength: 128),
                        ClaimType = c.String(),
                        ClaimValue = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.UserLogins",
                c => new
                    {
                        LoginProvider = c.String(nullable: false, maxLength: 128),
                        ProviderKey = c.String(nullable: false, maxLength: 128),
                        UserId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.LoginProvider, t.ProviderKey, t.UserId })
                .ForeignKey("dbo.Users", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.UserRoles",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        RoleId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.UserId, t.RoleId })
                .ForeignKey("dbo.Users", t => t.UserId, cascadeDelete: true)
                .ForeignKey("dbo.Roles", t => t.RoleId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.RoleId);
            
            CreateTable(
                "dbo.GamePlatforms",
                c => new
                    {
                        Code = c.Int(nullable: false),
                        Description = c.String(),
                    })
                .PrimaryKey(t => t.Code);
            
            CreateTable(
                "dbo.Game",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        AvaialblePlatforms = c.Int(nullable: false),
                        Description = c.String(),
                        ReleaseDate = c.DateTime(),
                        Genres = c.Int(nullable: false),
                        MediaForm = c.Int(nullable: false),
                        Name = c.String(),
                        GamePlatform_Code = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.GamePlatforms", t => t.GamePlatform_Code)
                .Index(t => t.GamePlatform_Code);
            
            CreateTable(
                "dbo.Genres",
                c => new
                    {
                        Code = c.Int(nullable: false),
                        Description = c.String(),
                    })
                .PrimaryKey(t => t.Code);
            
            CreateTable(
                "dbo.MediaForms",
                c => new
                    {
                        Code = c.Int(nullable: false),
                        Description = c.String(),
                    })
                .PrimaryKey(t => t.Code);
            
            CreateTable(
                "dbo.MediaTypes",
                c => new
                    {
                        Code = c.Int(nullable: false),
                        Description = c.String(),
                    })
                .PrimaryKey(t => t.Code);
            
            CreateTable(
                "dbo.MoviePlatforms",
                c => new
                    {
                        Code = c.Int(nullable: false),
                        Description = c.String(),
                    })
                .PrimaryKey(t => t.Code);
            
            CreateTable(
                "dbo.Movie",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Cast = c.String(),
                        Director = c.String(),
                        AvailablePlatforms = c.Int(nullable: false),
                        Description = c.String(),
                        ReleaseDate = c.DateTime(),
                        Genres = c.Int(nullable: false),
                        MediaForm = c.Int(nullable: false),
                        Name = c.String(),
                        MoviePlatform_Code = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.MoviePlatforms", t => t.MoviePlatform_Code)
                .Index(t => t.MoviePlatform_Code);
            
            CreateTable(
                "dbo.Roles",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Name = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "RoleNameIndex");
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.UserRoles", "RoleId", "dbo.Roles");
            DropForeignKey("dbo.Movie", "MoviePlatform_Code", "dbo.MoviePlatforms");
            DropForeignKey("dbo.Collection", "MediaType_Code", "dbo.MediaTypes");
            DropForeignKey("dbo.Game", "GamePlatform_Code", "dbo.GamePlatforms");
            DropForeignKey("dbo.UserRoles", "UserId", "dbo.Users");
            DropForeignKey("dbo.UserLogins", "UserId", "dbo.Users");
            DropForeignKey("dbo.Collection", "UserId", "dbo.Users");
            DropForeignKey("dbo.UserClaims", "UserId", "dbo.Users");
            DropForeignKey("dbo.CollectionDetail", "CollectionId", "dbo.Collection");
            DropForeignKey("dbo.Book", "BookPlatform_Code", "dbo.BookPlatforms");
            DropIndex("dbo.Roles", "RoleNameIndex");
            DropIndex("dbo.Movie", new[] { "MoviePlatform_Code" });
            DropIndex("dbo.Game", new[] { "GamePlatform_Code" });
            DropIndex("dbo.UserRoles", new[] { "RoleId" });
            DropIndex("dbo.UserRoles", new[] { "UserId" });
            DropIndex("dbo.UserLogins", new[] { "UserId" });
            DropIndex("dbo.UserClaims", new[] { "UserId" });
            DropIndex("dbo.Users", "UserNameIndex");
            DropIndex("dbo.Collection", new[] { "MediaType_Code" });
            DropIndex("dbo.Collection", new[] { "UserId" });
            DropIndex("dbo.CollectionDetail", new[] { "CollectionId" });
            DropIndex("dbo.Book", new[] { "BookPlatform_Code" });
            DropTable("dbo.Roles");
            DropTable("dbo.Movie");
            DropTable("dbo.MoviePlatforms");
            DropTable("dbo.MediaTypes");
            DropTable("dbo.MediaForms");
            DropTable("dbo.Genres");
            DropTable("dbo.Game");
            DropTable("dbo.GamePlatforms");
            DropTable("dbo.UserRoles");
            DropTable("dbo.UserLogins");
            DropTable("dbo.UserClaims");
            DropTable("dbo.Users");
            DropTable("dbo.Collection");
            DropTable("dbo.CollectionDetail");
            DropTable("dbo.Book");
            DropTable("dbo.BookPlatforms");
        }
    }
}
