﻿using Data.Entity;
using Library.Enum;

namespace Data.DTO
{
    public class CollectionDTO : BaseEntity
    {
        public bool Favourite { get; set; }

        public MediaTypeCode CollectionType { get; set; }

        public string UserId { get; set; }
        public int ItemCount { get; set; }

    }
}
