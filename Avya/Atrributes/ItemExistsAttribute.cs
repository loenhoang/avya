﻿using System.ComponentModel.DataAnnotations;
using System.Linq;
using Avya.Models;
using Data.DbContext;

namespace Avya.Atrributes
{
    public class ItemExistsAttribute : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            AvyaDbContext db = new AvyaDbContext();
            var model = validationContext.ObjectInstance as CollectionDetailModel;
            var itemExists = db.CollectionDetails.Where(x => x.CollectionId == model.CollectionId).Any(cd => cd.ItemName == model.ItemName);

            if (!itemExists)
                return ValidationResult.Success;
            else
                return new ValidationResult("You already own this item");
        }
    }
}