﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Avya.Startup))]
namespace Avya
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
