﻿using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Avya.Atrributes;
using Library.Enum;
using Library.Extension;

namespace Avya.Models
{
    public class CollectionDetailModel : BaseEntityModel
    {
        [Display(Name = "Rating")]
        [Range(0, 10)]
        [Required]
        public int Rating { get; set; }
        [Display(Name = "Book Platform")]
        public BookPlatformCode BookPlatformCode { get; set; }
        [Display(Name = "Platform")]
        public string BookPlatform { get { return BookPlatformCode.GetDescription(); } }

        [Display(Name = "Game Platform")]
        public GamePlatformCode GamePlatformCode { get; set; }
        [Display(Name = "Platform")]
        public string GamePlatform { get { return GamePlatformCode.GetDescription(); } }

        [Display(Name = "Movie Platform")]
        public MoviePlatformCode MoviePlatformCode { get; set; }
        [Display(Name = "Platform")]
        public string MoviePlatform { get { return MoviePlatformCode.GetDescription(); } }
        //[HiddenInput]
        //[Required]
        public int ItemId { get; set; }
        [Display(Name = "Item Name")]
        [Required]
        [ItemExists]
        public string ItemName { get; set; }
        [HiddenInput]
        [Required]
        public int CollectionId { get; set; }
    }
}