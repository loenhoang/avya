﻿using Library.Enum;
using Library.Extension;

namespace Avya.Models
{
    public class BookPlatformModel
    {
        public BookPlatformCode Code { get; set; }
        public string Description {
            get { return Code.GetDescription(); }
            set { }
        }
    }
}