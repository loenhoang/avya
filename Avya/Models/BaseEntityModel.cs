﻿namespace Avya.Models
{
    public class BaseEntityModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}