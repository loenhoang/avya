﻿using Library.Enum;

namespace Avya.Models
{
    public class MovieViewModel : BaseItemModel
    {
        public string Cast { get; set; }
        public string Director { get; set; }
        public MoviePlatformCode AvailablePlatforms { get; set; }
    }
}