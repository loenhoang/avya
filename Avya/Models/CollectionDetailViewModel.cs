﻿using System.Collections.Generic;
using Library.Enum;

namespace Avya.Models
{
    public class CollectionDetailViewModel
    {
        public MediaTypeCode Type { get; set; }
        public string CollectionName { get; set; }
        public IEnumerable<CollectionDetailModel> CollectionDetails { get; set; }
    }
}