﻿namespace Avya.Models
{
    public class SearchModel
    {
        public int Cid { get; set; }
        public int Type { get; set; }
        public string Name { get; set; }
        public string Item { get; set; }
        public string Search { get; set; }
    }

    public class ItemSearchModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string OriginalName { get; set; }
        public string Search { get; set; }
    }
}