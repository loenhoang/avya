﻿using Library.Enum;
using Library.Extension;

namespace Avya.Models
{
    public class MediaTypeModel
    {
        public MediaTypeCode Code { get; set; }
        public string Description {
            get { return Code.GetDescription(); }
            set { }
        }
    }
}