﻿using Library.Enum;
using Library.Extension;

namespace Avya.Models
{
    public class GamePlatformModel
    {
        public GamePlatformCode Code { get; set; }
        public string Description {
            get { return Code.GetDescription(); }
            set { }
        }
    }
}