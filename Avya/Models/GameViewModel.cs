﻿using Library.Enum;

namespace Avya.Models
{
    public class GameViewModel : BaseItemModel
    {
        public GamePlatformCode AvailablePlatforms { get; set; }
    }
}