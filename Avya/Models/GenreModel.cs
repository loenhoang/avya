﻿using Library.Enum;
using Library.Extension;

namespace Avya.Models
{
    public class GenreModel : BaseEntityModel
    {
        public GenreCode Code { get; set; }
        public string Description { get { return Code.GetDescription(); } set { } }
    }
}