﻿using Library.Enum;
using Library.Extension;

namespace Avya.Models
{
    public class MediaFormModel
    {
        public MediaFormCode Code { get; set; }
        public string Description {
            get { return Code.GetDescription(); }
            set { }
        }
    }
}