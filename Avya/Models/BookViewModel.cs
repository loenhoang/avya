﻿using Library.Enum;

namespace Avya.Models
{
    public class BookViewModel : BaseItemModel
    {
        public int ISBN { get; set; }
        public string Author { get; set; }
        public BookPlatformCode AvailablePlatforms { get; set; }
    }
}