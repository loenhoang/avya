﻿using System.ComponentModel.DataAnnotations;
using Library.Enum;

namespace Avya.Models
{
    public class CollectionViewModel : BaseEntityModel
    {
        public bool? Favourite { get; set; }

        [Required(ErrorMessage = "Please select a collection type")]
        [Display(Name = "Type")]
        public MediaTypeCode? CollectionType { get; set; }

        [Display(Name = "No. of Items")]
        public int ItemCount { get; set; }

        public string UserId { get; set; }
    }
}