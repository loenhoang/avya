﻿using Library.Enum;
using Library.Extension;

namespace Avya.Models
{
    public class MoviePlatformModel
    {
        public MoviePlatformCode Code { get; set; }
        public string Description {
            get { return Code.GetDescription(); }
            set { }
        }
    }
}