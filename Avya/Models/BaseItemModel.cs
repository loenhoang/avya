﻿using System;
using System.ComponentModel.DataAnnotations;
using Library.Enum;

namespace Avya.Models
{
    public class BaseItemModel : BaseEntityModel
    {
        public string Description { get; set; }
        [Display(Name = "Release Date")]
        public DateTime? ReleaseDate { get; set; }
        public GenreCode Genres { get; set; }
        public MediaFormCode MediaForm { get; set; }
    }
}