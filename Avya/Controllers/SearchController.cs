﻿using System.Linq;
using System.Web.Mvc;
using Avya.Models;
using Data.DbContext;
using Data.Extension;
using Library.Enum;

namespace Avya.Controllers
{
    [Authorize]
    public class SearchController : BaseController
    {
        public SearchController(AvyaDbContext _db) : base(_db)
        {
        }

        // GET: Search
        [HttpPost]
        [ValidateAntiForgeryToken]
        public PartialViewResult Global(string s)
        {
            var results = (from c in db.Collections
                           join cd in db.CollectionDetails
                           on c.Id equals cd.CollectionId
                           into joined
                           from scd in joined.DefaultIfEmpty()
                           where (c.UserId == UserId) && (c.Name.Contains(s) || scd.ItemName.Contains(s))
                           select new SearchModel()
                           {
                               Cid = c.Id,
                               Type = (int)c.CollectionType,
                               Name = c.Name,
                               Item = scd.ItemName,
                               Search = s
                           }).ToList();

            return PartialView("_GlobalSearchPartial", results);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public PartialViewResult Item(string s, int type)
        {
            var t = (MediaTypeCode)type;
            var results = db.CollectionByType(t)
                            .Where(x => x.Name.Contains(s))
                            .Take(10)
                            .Select(i => new ItemSearchModel { Id = i.Id, Name = i.Name, OriginalName = i.Name, Search = s })
                            .ToList();

            return PartialView("_ItemSearchPartial", results);
        }
    }
}