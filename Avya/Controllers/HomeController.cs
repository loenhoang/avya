﻿using System.Web.Mvc;
using Data.DbContext;

namespace Avya.Controllers
{
    public class HomeController : BaseController
    {
        public HomeController(AvyaDbContext _db) : base(_db)
        {
        }

        public ActionResult Index()
        {
            if (Request.IsAuthenticated)
            {
                return RedirectToAction("Index", "Collection");
            }
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}