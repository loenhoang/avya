﻿using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
using Avya.Models;
using Data.DbContext;
using Data.DTO;
using Data.Entity;
using Library.Mapper;

namespace Avya.Controllers
{
    [Authorize]
    public class CollectionController : BaseController
    {
        public CollectionController(AvyaDbContext _db) : base(_db)
        {
        }

        // GET: Collection
        public ActionResult Index()
        {
            var results = db.GetAllUserCollections(UserId)
                        .Select(x => Mapper.Map<CollectionDTO, CollectionViewModel>(x))
                        .ToList();

            return View(results);
        }
        public ActionResult Create()
        {
            return View();
        }

        // POST: Collection/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,CollectionType,UserId,Name")] CollectionViewModel collectionViewModel)
        {
            if (ModelState.IsValid)
            {
                var entity = Mapper.Map<CollectionViewModel, Collection>(collectionViewModel);
                db.Collections.Add(entity);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(collectionViewModel);
        }

        // POST: Collection/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,CollectionType,UserId,Name,Favourite")] CollectionViewModel collectionViewModel)
        {
            if (ModelState.IsValid)
            {
                var c = Mapper.Map<CollectionViewModel, Collection>(collectionViewModel);
                db.Entry(c).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(collectionViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult Delete(int id)
        {
            bool isSuccess = true;
            if (ModelState.IsValid)
            {
                db.Entry(new Collection { Id = id }).State = EntityState.Deleted;
                db.SaveChanges();
            }
            return Json(isSuccess);
        }
    }
}
