﻿using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Avya.Models;
using Data.DbContext;
using Microsoft.AspNet.Identity;
using Microsoft.Owin;
using Microsoft.Owin.Security;

namespace Avya.Controllers
{
    public class BaseController : Controller
    {
        protected string UserId
        {
            get { return User.Identity.GetUserId(); }
        }

        protected AvyaDbContext db;
        public BaseController(AvyaDbContext _db)
        {
            db = _db;
        }

        [Authorize]
        public PartialViewResult DeletePartialView(string n, int id, string e)
        {
            if (e.ToLower() == "c")
            {
                return PartialView("_DeleteCollectionPartial", new CollectionViewModel { Id = id, Name = n });
            }
            if (e.ToLower() == "cd")
            {
                return PartialView("_DeleteCollectionDetailPartial", new CollectionDetailModel { Id = id, ItemName = n });

            }
            return PartialView("Error");
        }

        protected async Task RefreshClaims(IAuthenticationManager manager, string claimType, string claimValue)
        {
            // http://stackoverflow.com/a/22445883
            var identity = (ClaimsIdentity)User.Identity;
            var claim = identity.Claims.FirstOrDefault(r => r.Type == claimType);

            if (claim != null)
                identity.RemoveClaim(claim);

            identity.AddClaim(new Claim(claimType, claimValue));

            IOwinContext ctx = Request.GetOwinContext();

            var authenticationContext =
                await ctx.Authentication.AuthenticateAsync(DefaultAuthenticationTypes.ApplicationCookie);

            if (authenticationContext != null)
            {
                manager.AuthenticationResponseGrant = new AuthenticationResponseGrant(
                    identity,
                    authenticationContext.Properties);
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }

            base.Dispose(disposing);
        }

    }
}