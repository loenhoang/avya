﻿using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using Avya.Models;
using Data.DbContext;
using Data.Entity;
using Library.Enum;
using Library.Mapper;

namespace Avya.Controllers
{
    [RoutePrefix("Collection/Detail")]
    public class CollectionDetailController : BaseController
    {
        public CollectionDetailController(AvyaDbContext _db) : base(_db)
        {
        }

        [Route(Name = "DetailIndex")]
        // GET: CollectionDetail
        public ActionResult Index(int? cid, int? t, string c = null)
        {
            if (cid == null || t == null)
            {
                return RedirectToAction("Index", "Collection");
            }
            var result = new CollectionDetailViewModel
            {
                Type = (MediaTypeCode)t,
                CollectionName = c ?? db.Collections.Where(x => x.Id == cid).FirstOrDefault().Name,
                CollectionDetails = db.GetAllCollectionDetails((int)cid, (MediaTypeCode)t)
                                    .Select(cd => Mapper.Map<CollectionDetail, CollectionDetailModel>(cd)).ToList()
            };

            return View(result);
        }
        [Route("Create", Name = "CreateDetailView")]
        public ActionResult Create()
        {
            return View();
        }

        // POST: CollectionDetail/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Route(Name = "CreateDetailAction")]
        public ActionResult Create(
            [Bind(Include = "Id,Rating,CollectionId,ItemName, BookPlatformCode, GamePlatformCode, MoviePlatformCode")]
            CollectionDetailModel model)
        {
            if (ModelState.IsValid)
            {
                model.ItemId = GetItemId(model);
                db.CollectionDetails.Add(Mapper.Map<CollectionDetailModel, CollectionDetail>(model));
                db.SaveChanges();
                var collectionName = db.Collections.FirstOrDefault(c => c.Id == model.CollectionId).Name;
                return RedirectToAction("Index", "CollectionDetail", new { cid = model.CollectionId, t = GetMediaType(model), c = collectionName });
            }

            return View(model);
        }
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            return View();
        }


        // POST: CollectionDetail/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,ItemId,ItemName, CollectionId, BookPlatformCode, GamePlatformCode, MoviePlatformCode,Rating")] CollectionDetailModel model)
        {
            ModelState.Remove("ItemName");
            if (ModelState.IsValid)
            {
                var cd = Mapper.Map<CollectionDetailModel, CollectionDetail>(model);

                db.Entry(cd).State = EntityState.Modified;
                db.SaveChanges();
                var collectionName = db.Collections.FirstOrDefault(c => c.Id == model.CollectionId).Name;
                return RedirectToAction("Index", new { cid = cd.CollectionId, t = GetMediaType(model), c = collectionName });
            }
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult Delete(int id)
        {
            bool isSuccess = true;
            if (ModelState.IsValid)
            {
                db.Entry(new CollectionDetail { Id = id }).State = EntityState.Deleted;
                db.SaveChanges();
            }
            return Json(isSuccess);
        }

        public bool IsCollectionEmpty(int cid)
        {
            var result = !db.CollectionDetails.Any(x => x.CollectionId == cid);
            return result;
        }

        #region Private

        private int GetMediaType(CollectionDetailModel model)
        {
            if (model.BookPlatformCode != 0)
                return (int)MediaTypeCode.Book;
            if (model.GamePlatformCode != 0)
                return (int)MediaTypeCode.Game;
            return (int)MediaTypeCode.Movie;
        }

        //TODO Write service to fill book, game, and movie tables
        private int GetItemId(CollectionDetailModel model)
        {
            var id = 0;
            if (model.BookPlatformCode != 0)
            {
                if (!db.Books.Any(x => x.Name == model.ItemName))
                {
                    db.Books.Add(new Book { Name = model.ItemName });
                    db.SaveChanges();
                }
                id = db.Books.Where(x => x.Name == model.ItemName).FirstOrDefault().Id;
            }
            else if (model.GamePlatformCode != 0)
            {
                if (!db.Games.Any(x => x.Name == model.ItemName))
                {
                    db.Games.Add(new Game { Name = model.ItemName });
                    db.SaveChanges();
                }
                id = db.Games.Where(x => x.Name == model.ItemName).FirstOrDefault().Id;
            }
            else
            {
                if (!db.Movies.Any(x => x.Name == model.ItemName))
                {
                    db.Movies.Add(new Movie { Name = model.ItemName });
                    db.SaveChanges();
                }
                id = db.Movies.Where(x => x.Name == model.ItemName).FirstOrDefault().Id;

            }
            return id;
        }

        #endregion
    }
}
