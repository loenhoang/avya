﻿using System.Globalization;
using System.Linq;
using System.Web.Mvc;
using System.Web.UI;
using Data.DbContext;

namespace Avya.Controllers
{
    [OutputCache(Location = OutputCacheLocation.None, NoStore = true)]
    public class ValidationController : BaseController
    {
        public ValidationController(AvyaDbContext _db) : base(_db)
        {
        }

        public JsonResult IsUserExist(string userName)
        {

            if (!db.Users.Any(u => u.UserName == userName))
                return Json(true, JsonRequestBehavior.AllowGet);

            string suggestedUsername = string.Format(CultureInfo.InvariantCulture,
                "{0} is not available.", userName);

            for (int i = 1; i < 100; i++)
            {
                string altCandidate = userName + i.ToString();
                if (!db.Users.Any(u => u.UserName == altCandidate))
                {
                    suggestedUsername = string.Format(CultureInfo.InvariantCulture,
                   "{0} is not available. Try {1}.", userName, altCandidate);
                    break;
                }
            }
            return Json(suggestedUsername, JsonRequestBehavior.AllowGet);
        }
    }
}