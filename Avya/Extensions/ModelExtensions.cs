﻿using System.Collections.Generic;
using Avya.Models;

namespace Avya.Extensions
{
    public static class ModelExtensions
    {
        public static string GetEntityName<T>(this IEnumerable<T> model) where T : BaseEntityModel
        {
            var name = model.GetType().GetGenericArguments()[0].ToString();
            name = name.Replace("Avya.Models.", string.Empty).Replace("ViewModel", string.Empty).Replace("Model", string.Empty);
            return name;
        }
    }
}