﻿using System;
using System.Collections.Generic;
using System.Linq;
using Avya.Models;
using Library.Enum;

namespace Avya.Extensions
{
    public static class IEnumerableExtensions
    {
        public static List<T> SortAndOrderBy<T>(this IEnumerable<T> list, string sort, string order) where T : BaseEntityModel
        {
            var getProperty = (typeof(T) == typeof(CollectionViewModel)) ?
                (Func<string, BaseEntityModel, object>)GetCollectionProperty :
                GetCollectionDetailProperty;

            var result = IsDesc(order) ?
                        list.OrderByDescending(c => getProperty(sort, c)).ToList() :
                        list.OrderBy(c => getProperty(sort, c)).ToList();

            return result;
        }

        #region Private

        private static object GetCollectionProperty(string s, BaseEntityModel m)
        {
            var model = m as CollectionViewModel;
            var sort = string.IsNullOrWhiteSpace(s) ? "name" : s;
            if ((SortCode)Enum.Parse(typeof(SortCode), sort, true) == SortCode.Fav)
            {
                return model.Favourite;
            }
            else if ((SortCode)Enum.Parse(typeof(SortCode), sort, true) == SortCode.Items)
            {
                return model.ItemCount;
            }
            return model.Name;
        }

        private static object GetCollectionDetailProperty(string s, BaseEntityModel m)
        {
            var model = m as CollectionDetailModel;
            var sort = string.IsNullOrWhiteSpace(s) ? "name" : s;
            if ((SortCode)Enum.Parse(typeof(SortCode), sort, true) == SortCode.Platform)
            {
                if (model.BookPlatformCode != 0)
                    return model.BookPlatform;
                if (model.GamePlatformCode != 0)
                    return model.GamePlatform;
                return model.MoviePlatform;
            }
            else if ((SortCode)Enum.Parse(typeof(SortCode), sort, true) == SortCode.Rating)
            {
                return model.Rating;
            }

            return model.ItemName;
        }

        private static bool IsDesc(string order)
        {
            if (string.IsNullOrWhiteSpace(order))
            {
                return false;
            }

            var result = (OrderByCode)Enum.Parse(typeof(OrderByCode), order, true) == OrderByCode.Desc;
            return result;
        }

        #endregion
    }
}