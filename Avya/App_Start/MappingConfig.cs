﻿using Avya.Models;
using Data.DTO;
using Data.Entity;
using Library.Enum;
using Library.Mapper;

namespace Avya.App_Start
{
    public static class MappingConfig
    {
        public static void AddMappings()
        {
            Mapper.AddMap<BookPlatform, BookPlatformModel>();
            Mapper.AddMap<CollectionDetail, CollectionDetailModel>(
                cd => new CollectionDetailModel
                {
                    Id = cd.Id,
                    Name = cd.Name,
                    CollectionId = cd.CollectionId,
                    ItemId = cd.ItemId,
                    ItemName = cd.ItemName,
                    Rating = cd.Rating,
                    BookPlatformCode = cd.BookPlatform,
                    GamePlatformCode = cd.GamePlatform,
                    MoviePlatformCode = cd.MoviePlatform
                });

            Mapper.AddMap<CollectionDetailModel, CollectionDetail>(
                cd => new CollectionDetail
                {
                    Id = cd.Id,
                    Name = cd.Name,
                    CollectionId = cd.CollectionId,
                    ItemId = cd.ItemId,
                    ItemName = cd.ItemName,
                    Rating = cd.Rating,
                    BookPlatform = cd.BookPlatformCode,
                    GamePlatform = cd.GamePlatformCode,
                    MoviePlatform = cd.MoviePlatformCode
                });

            Mapper.AddMap<GamePlatform, GamePlatformModel>();
            Mapper.AddMap<Genre, GenreModel>();
            Mapper.AddMap<MediaForm, MediaFormModel>();
            Mapper.AddMap<MediaType, MediaTypeModel>();
            Mapper.AddMap<MoviePlatform, MoviePlatformModel>();
            Mapper.AddMap<Book, BookViewModel>();
            Mapper.AddMap<Game, GameViewModel>();
            Mapper.AddMap<Movie, MovieViewModel>();

            Mapper.AddMap<CollectionDTO, CollectionViewModel>(
                from => new CollectionViewModel
                {
                    Id = from.Id,
                    Name = from.Name,
                    UserId = from.UserId,
                    CollectionType = from.CollectionType,
                    Favourite = from.Favourite,
                    ItemCount = from.ItemCount
                });

            Mapper.AddMap<CollectionViewModel, Collection>(
                from => new Collection
                {
                    Id = from.Id,
                    Name = from.Name,
                    UserId = from.UserId,
                    CollectionType = (MediaTypeCode)from.CollectionType,
                    Favourite = from.Favourite ?? false
                });
        }
    }
}