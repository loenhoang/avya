﻿using System;
using System.Web;
using Library.Enum;
using Library.Extension;

namespace Avya.Helpers
{
    public class QueryStringHelper
    {
        public static bool ValidQueryStrings(HttpRequestBase request, params string[] queryStrings)
        {
            var result = true;
            foreach (var q in queryStrings)
            {
                int qs;
                result = result && request.QueryString[q] != null;
                if (q == "t" || q == "cid")
                {
                    var isInt = int.TryParse(request.QueryString[q], out qs);
                    if (!isInt)
                    {
                        return false;
                    }
                    result = result && isInt;
                }
            }
            return result;
        }

        public static bool IsValidSortAndOrder(string sort, string order)
        {
            var result = !string.IsNullOrWhiteSpace(sort) && !string.IsNullOrWhiteSpace(order) &&
                Enum.IsDefined(typeof(SortCode), sort.ToTitleCase()) &&
                Enum.IsDefined(typeof(OrderByCode), order.ToTitleCase());

            return result;
        }
    }
}